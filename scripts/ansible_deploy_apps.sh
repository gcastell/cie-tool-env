#!/bin/sh

set -e

playbooks_path=~/My_Projects/gitlab_public/cie-tool-env/configuration-as-code/ansible/playbooks
vagrant_home=~/My_Projects/gitlab_public/cie-tool-env/infrastructure-as-code/vagrant

echo " ---- change directory to ansible playbooks home ---- "
cd ${playbooks_path}
pwd

echo "  ----- Starting all application Deployments..... ------"

# echo "  ----- deploying Jenkins Server VM ... -----  "
# ansible-playbook install_config_jenkins_vm.yml -i hosts
# echo "  ----- Completed the deplyment of the Jenkins Server VM ... -----  "

echo "  ----- deploying Jenkins Server VM ... -----  "
ansible-playbook install_config_jenkins_vm.yml -i hosts
echo "  ----- Completed the deplyment of the Jenkins Server VM ... -----  "
#
# echo "  ----- Deploying Nexus Server VM -----  "
# ansible-playbook install_config_nexus_server.yml -i hosts
# echo "  ----- Completed the deployment of the Nexus Server VM ... -----  "
#
# echo "  ----- deploying gitlab Server VM -----  "
# ansible-playbook install_config_gitlab.yml -i hosts
# echo "  ----- Completed the deployment of the Vagrant gitlab Server VM ... -----  "

#echo "  ----- deploying Nginx Server VM -----  "
#ansible-playbook install_config_nginx_server.yml -i hosts
#echo "  ----- Completed the deployment of the Nginx Server VM ... -----  "

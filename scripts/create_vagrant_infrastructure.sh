#!/bin/sh

set -e

project_path=~/My_Projects/gitlab_public/cie-tool-env
vagrant_path="${project_path}/infrastructure-as-code/vagrant"

echo " ---- change directory to vagrant home ---- "
cd $vagrant_path
pwd

echo "  ----- Creating the CIE infrastructure ..... ------"

#echo "  ----- Creating Vagrant Jenkins Server VM ... -----  "
#vagrant destroy -f jenkins-vm && vagrant up jenkins-vm
#echo "  ----- Completed the creation of the Vagrant Jenkins Server VM ... -----  "

echo "  ----- Creating Vagrant Jenkins Server VM ... -----  "
vagrant destroy -f jenkins-vm-server && vagrant up jenkins-vm-server
echo "  ----- Completed the creation of the Vagrant Jenkins Server VM ... -----  "

# echo "  ----- Create Vagrant Nexus Server VM -----  "
# vagrant destroy -f nexus && vagrant up nexus
# echo "  ----- Completed the creation of the Vagrant Nexus Server VM ... -----  "
#
# echo "  ----- Create Vagrant Gitlab Server VM -----  "
# vagrant destroy -f gitlab && vagrant up gitlab
# echo "  ----- Completed the creation of the Vagrant gitlab Server VM ... -----  "

#echo "  ----- Create Vagrant Nginx Server VM -----  "
#vagrant destroy -f nginx && vagrant up nginx
#echo "  ----- Completed the creation of the Vagrant Ngix Server VM ... -----  "
